#ifndef __LoopSubdivision_h__
#define __LoopSubdivision_h__
#include <unordered_map>
#include <vector>
#include "Mesh.h"

inline void LoopSubdivision(TriangleMesh<3>& mesh)
{
	std::vector<Vector3>& old_vtx=mesh.Vertices();
	std::vector<Vector3i>& old_tri=mesh.Elements();
	std::vector<Vector3> new_vtx;		////vertex array for the new mesh
	std::vector<Vector3i> new_tri;	////element array for the new mesh

	new_vtx=old_vtx;	////copy all the old vertices to the new_vtx array

	////step 1: add mid-point vertices and triangles
	////for each old triangle,
	////add three new vertices (in the middle of each edge) to new_vtx
	////add four new triangles to new_tri

	/*your implementation here*/
	std::unordered_map<Vector2i,int> edge_vtx_map;
	std::unordered_map<Vector2i,Vector2i> edge_tri_map;
	std::unordered_map<int,std::vector<int> > vtx_vtx_map;

	for(int i = 0; i < old_tri.size(); i++){
		Vector3i tri = old_tri[i];
		std::vector<int> vtx{tri[0],tri[1], tri[2]};
		std::vector<Vector2i> edges;
		edges.push_back(Sorted(Vector2i(vtx[0], vtx[1])));
		edges.push_back(Sorted(Vector2i(vtx[1], vtx[2])));
		edges.push_back(Sorted(Vector2i(vtx[2], vtx[0])));

		// loop over original vertices
		for(int j = 0; j < 3; j++){
			// populate vtx_vtx_map
			if(vtx_vtx_map.count(vtx[j]) == 0) { // vertex not in map
				std::vector<int> temp;
				temp.push_back(vtx[(j + 1) % 3]);
				temp.push_back(vtx[(j + 2) % 3]);
				vtx_vtx_map[vtx[j]] = temp;
			}
			else { // vertex is in map
				vtx_vtx_map[vtx[j]].push_back(vtx[(j + 1) % 3]);
				vtx_vtx_map[vtx[j]].push_back(vtx[(j + 2) % 3]);
			}
		}

		// loop over edges
		for(int j = 0; j < 3; j++){
			// populate edge_tri_map
			if(edge_tri_map.count(edges[j]) == 0) { // edge not in map
				edge_tri_map[edges[j]] = Vector2i(i, -1);
			}
			else { // edge in map
				Vector2i temp = edge_tri_map[edges[j]];
				temp[1] = i;
				edge_tri_map[edges[j]] = temp;
			}

			// populate edge_vtx_map and add midpoints
			if(edge_vtx_map.count(edges[j]) == 0) { // edge not in map
				Vector3 p1 = old_vtx[edges[j][0]];
				Vector3 p2 = old_vtx[edges[j][1]];
				Vector3 mid = (p1 + p2) / 2.;
				// add midpoint
				new_vtx.push_back(mid);
				vtx.push_back(new_vtx.size() - 1);
				edge_vtx_map[edges[j]] = new_vtx.size() - 1;
			}
			else { // else the edge is in the map, and its midpoint is already added
				vtx.push_back(edge_vtx_map[edges[j]]);
			}
		}

		// add new triangles
		new_tri.push_back(Vector3i(vtx[0], vtx[5], vtx[3]));
		new_tri.push_back(Vector3i(vtx[3], vtx[4], vtx[1]));
		new_tri.push_back(Vector3i(vtx[5], vtx[2], vtx[4]));
		new_tri.push_back(Vector3i(vtx[3], vtx[5], vtx[4]));
	}

	std::vector<Vector3> temp_pos = new_vtx;

	////step 2: update the position for each new mid-point vertex:
	////for each mid-point vertex, find its two end-point vertices A and B,
	////and find the two opposite-side vertices on the two incident triangles C and D,
	////then update the new position as .375*(A+B)+.125*(C+D)

	/*your implementation here*/
	for(auto& iter:edge_vtx_map){
		Vector2i e = iter.first;
		Vector2i ts = edge_tri_map[e];
		int v = iter.second;
		// end-points
		int a = e[0];
		int b = e[1];
		// opposite side points
		Vector3i t1 = old_tri[ts[0]];
		Vector3i t2 = old_tri[ts[1]];
		int c = -1;
		int d = -1;
		for(int i = 0; i < 3; i++){
			if(a != t1[i] && b != t1[i]) {
				c = t1[i];
			}
			if(a != t2[i] && b != t2[i]) {
				d = t2[i];
			}
		}

		// update position
		temp_pos[v] = .375 * (new_vtx[a] + new_vtx[b]) + .125 * (new_vtx[c] + new_vtx[d]);
	}

	////step 3: update vertices of each old vertex
	////for each old vertex, find its incident old vertices, and update its position according its incident vertices

	/*your implementation here*/
	for(auto& iter:vtx_vtx_map){
		int center = iter.first;
		std::vector<int> neighbors = iter.second;
		int n = neighbors.size();
		real beta;
		if(n > 3) {
			beta = 3. / (8. * real(n));
		}
		else {
			beta = 3. / 16.;
		}
		temp_pos[center] = (1 - real(n) * beta) * new_vtx[center];
		for(auto& neighbor:neighbors){
			temp_pos[center] += beta * new_vtx[neighbor];
		}
	}

	new_vtx = temp_pos;

	////update subdivided vertices and triangles onto the input mesh
	old_vtx=new_vtx;
	old_tri=new_tri;
}

#endif
