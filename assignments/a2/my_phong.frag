/*This is your first fragment shader!*/

#version 330 core

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/* Passed time from the begining of the program */
uniform float iTime;

/*input variables*/
in vec4 vtx_color;
////TODO [Step 2]: add your in variables from the vertex shader
in vec4 frag_pos;
in vec4 vtx_pos;
in vec4 vtx_norm;

/*output variables*/
out vec4 frag_color;

/*hard-coded lighting parameters*/
const vec3 LightPosition1 = vec3(-3, 1, 1); // blue
const vec3 LightPosition2 = vec3(-1, -1, -5); // red
////TODO [Step 3]: add your Phong lighting parameters here
const float ka = 0.05f;
const vec3 l1_color = vec3(0.f,0.f,1.f);
const vec3 l2_color = vec3(1.f,0.f,0.f);
const float kd = 0.75f;
const float ks = 0.4f;
const int p = 15;

void main()
{
	////TODO [Step 3]: add your Phong lighting calculation
	vec3 light_dir1 = normalize(LightPosition1 - frag_pos.xyz);
	vec3 light_dir2 = normalize(LightPosition2 - frag_pos.xyz);
	vec3 norm = normalize(vtx_norm.xyz);
	vec4 view_pos = pvm * position;
	vec3 v = normalize(position.xyz - frag_pos.xyz);
	vec3 r1 = normalize(-light_dir1 + 2.f * dot(light_dir1, norm) * norm);
	vec3 r2 = normalize(-light_dir2 + 2.f * dot(light_dir2, norm) * norm);

	vec3 ambient1 = ka*l1_color;
	vec3 diffuse1 = kd*l1_color*max(dot(norm, light_dir1), 0.0);
	vec3 specular1 = ks*l1_color*pow(max(dot(v,r1), 0.0), p);

	vec3 ambient2 = ka*l2_color;
	vec3 diffuse2 = kd*l2_color*max(dot(norm, light_dir2), 0.0);
	vec3 specular2 = ks*l2_color*pow(max(dot(v,r2), 0.0), p);

	frag_color = vec4((ambient1 + diffuse1 + specular1 + ambient2 + diffuse2 + specular2) * vtx_color.rgb, 1.f);

	// frag_color = vec4(0.f,1.f,0.f,1.f);
}
