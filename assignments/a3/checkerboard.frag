/*This is your first fragment shader!*/

#version 330 core

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*input variables*/
//// TODO: declare the input fragment attributes here
in vec3 vtx_pos;

/*output variables*/
out vec4 frag_color;

void main()
{
	vec3 col = vec3(1.0);
	vec2 uv = vec2(0.0);

	//// TODO: produce a checkerboard texture on the sphere with the input vertex uv
	float phi = atan(vtx_pos.z/vtx_pos.x);
	float theta = acos(vtx_pos.y);
	float pi = 3.1415926535793;

	uv = vec2(phi / (2. * pi), theta / pi);
	float u_floor = floor(8.*uv.x);
	float v_floor = floor(4.*uv.y);
	if(mod(u_floor + v_floor, 2.0) == 0.0){
		col = vec3(1.f);
	}
	else{
		col = vec3(0.f);
	}

	frag_color = vec4(col, 1.0);
}
