#version 330 core

layout (std140) uniform camera
{
	mat4 projection;
	mat4 view;
	mat4 pvm;
	mat4 ortho;
	vec4 position;
};

/*uniform variables*/
uniform float iTime;					////time

/*input variables*/
in vec3 vtx_pos;

/*input variables*/
out vec4 frag_color;

///////////// Part 1a /////////////////////
/* Create a function that takes in an xy coordinate and returns a 'random' 2d vector. (There is no right answer)
   Feel free to find a hash function online. Use the commented function to check your result */
vec2 hash2(vec2 v)
{
	vec2 rand = vec2(0,0);

	// Your implementation starts here

	//rand  = 50.0 * 1.05 * fract(v * 0.3183099 + vec2(0.71, 0.113));
	//rand = -1.0 + 2 * 1.05 * fract(rand.x * rand.y * (rand.x + rand.y) * rand);

	// taken from https://en.wikipedia.org/wiki/Perlin_noise
	float random = 2920.f * sin(v.x * 21942.f + v.y * 171324.f + 8912.f) * cos(v.x * 23157.f * v.y * 217832.f + 9758.f);
 return vec2(cos(random), sin(random));
	// Your implementation ends here

	return rand;
}

///////////// Part 1b /////////////////////
/*  Using i, f, and m, compute the perlin noise at point v */
float perlin_noise(vec2 v)
{
	float noise = 0;
	// Your implementation starts here
	//   P2------------P3
	//   | \         / |
	//   |  \   /      |
	//   |   V_        |
	//   | /      _    |
	//   P0------------P1
	vec2 v_i = floor(v);
	vec2 v_f = fract(v);

	vec2 p0 = v_i;
	vec2 p1 = v_i + vec2(1.f,0.f);
	vec2 p2 = v_i + vec2(0.f,1.f);
	vec2 p3 = v_i + vec2(1.f,1.f);

	vec2 g0 = hash2(p0);
	vec2 g1 = hash2(p1);
	vec2 g2 = hash2(p2);
	vec2 g3 = hash2(p3);

	vec2 v0 = v - p0;
	vec2 v1 = v - p1;
	vec2 v2 = v - p2;
	vec2 v3 = v - p3;

	float n01 = mix(dot(v0, g0), dot(v1, g1), smoothstep(0.f,1.f,v_f.x));
	float n23 = mix(dot(v2, g2), dot(v3, g3), smoothstep(0.f,1.f,v_f.x));
	noise = mix(n01, n23, smoothstep(0.f,1.f,v_f.y));

	// Your implementation ends here
	return noise;
}

///////////// Part 1c /////////////////////
/*  Given a point v and an int num, compute the perlin noise octave for point v with octave num
	num will be greater than 0 */
float noiseOctave(vec2 v, int num)
{
	float sum = 0;
	// Your implementation starts here
	// sum = perlin_noise(v);

	for(int i = 0; i < num; i++){
		sum += pow(2, -i) * perlin_noise(pow(2, i)*v);
	}
	// Your implementation ends here
	return sum;
}

///////////// Part 2a /////////////////////
/* create a function that takes in a 2D point and returns a height using perlin noise
   There is no right answer. Think about what functions will create what shapes.
   If you want steep mountains with flat tops, use a function like sqrt(noiseOctave(v,num)).
   If you want jagged mountains, use a function like e^(noiseOctave(v,num))
   You can also add functions on top of each other and change the frequency of the noise
   by multiplying v by some value other than 1*/
float height(vec2 v){
	float h = 0;
	// Your implementation starts here
	h = noiseOctave(v, 6) / 2.f;
	float k = 15;
	h = 2.f / (1.f + exp(-k * h)) - 1.f;
	// Your implementation ends here
	return h;
}

///////////// Part 2b /////////////////////
/* compute the normal vector at v by find the points d to the left/right and d forward/backward
    and using a cross product. Be sure to normalize the result */
vec3 compute_normal(vec2 v, float d)
{
	vec3 normal_vector = vec3(0,0,0);
	// Your implementation starts here

	//     V3
	//     ^
	//     |
	//V2<--v-->V1
	//     |
	//     V
	//     V4

	vec3 v1 = vec3(v.x + d, v.y, height(vec2(v.x + d, v.y)));
	vec3 v2 = vec3(v.x - d, v.y, height(vec2(v.x - d, v.y)));
	vec3 v3 = vec3(v.x, v.y + d, height(vec2(v.x, v.y + d)));
	vec3 v4 = vec3(v.x, v.y - d, height(vec2(v.x, v.y - d)));

	normal_vector = normalize(cross(v1 - v2, v3 - v4));
	// Your implementation ends here
	return normal_vector;
}

///////////// Part 2c /////////////////////
/* complete the get_color function by setting emissiveColor using some function of height/normal vector/noise */
/* put your Phong/Lambertian lighting model here to synthesize the lighting effect onto the terrain*/
vec3 get_color(vec2 v)
{
	vec3 vtx_normal = compute_normal(v, 0.01);
	vec3 emissiveColor = vec3(0,0,0);
	vec3 lightingColor= vec3(1.,1.,1.);

	// Your implementation starts here
	float h = height(v);

	float hrange = 2.f;
	float bin_num = 13.f;
	vec3 c1 = vec3(0.f);
	vec3 c2 = vec3(1.f);
	int nn = 7;

	if(h > 6.f * hrange / bin_num){
		c1 = vec3(0.922, 0.325, 0.192);
		c2 = vec3(0.922, 0.435, 0.212);
		nn = 6;
	}
	else if(h > 5.f * hrange / bin_num){
		c1 = vec3(0.922, 0.435, 0.212);
		c2 = vec3(0.918, 0.545, 0.235);
		nn = 5;
	}
	else if(h > 4.f * hrange / bin_num){
		c1 = vec3(0.918, 0.545, 0.235);
		c2 = vec3(0.929, 0.667, 0.271);
		nn = 4;
	}
	else if(h > 3.f * hrange / bin_num){
		c1 = vec3(0.929, 0.667, 0.271);
		c2 = vec3(0.937, 0.796, 0.310);
		nn = 3;
	}
	else if(h > 2.f * hrange / bin_num){
		c1 = vec3(0.937, 0.796, 0.310);
		c2 = vec3(0.953, 0.922, 0.345);
		nn = 2;
	}
	else if(h > 1.f * hrange / bin_num){
		c1 = vec3(0.953, 0.922, 0.345);
		c2 = vec3(0.902, 0.980, 0.388);
		nn = 1;
	}
	else if(h > 0.f * hrange / bin_num){
		c1 = vec3(0.902, 0.980, 0.388);
		c2 = vec3(0.784, 0.980, 0.467);
		nn = 0;
	}
	else if(h > -1.f * hrange / bin_num){
		c1 = vec3(0.784, 0.980, 0.467);
		c2 = vec3(0.678, 0.973, 0.573);
		nn = -1;
	}
	// else if(h > -2.f * hrange / bin_num){
	// 	c1 = vec3(0.678, 0.973, 0.573);
	// 	c2 = vec3(0.580, 0.976, 0.678);
	// 	nn = -2;
	// }
	// else if(h > -3.f * hrange / bin_num){
	// 	c1 = vec3(0.580, 0.976, 0.678);
	// 	c2 = vec3(0.502, 0.973, 0.804);
	// 	nn = -3;
	// }
	// else if(h > -4.f * hrange / bin_num){
	// 	c1 = vec3(0.502, 0.973, 0.804);
	// 	c2 = vec3(0.455, 0.973, 0.922);
	// 	nn = -4;
	// }
	// else if(h > -5.f * hrange / bin_num){
	// 	c1 = vec3(0.455, 0.973, 0.922);
	// 	c2 = vec3(0.408, 0.902, 0.980);
	// 	nn = -5;
	// }
	// else if(h > -6.f * hrange / bin_num){
	else{
		// c1 = vec3(0.408, 0.902, 0.980);
		// c2 = vec3(0.345, 0.761, 0.973);
		c1 = vec3(0.110, 0.263, 0.459);
		c2 = vec3(0.110, 0.263, 0.459) * 0.7;
		nn = -6;
	}
	float fr = 0.0;
	if(nn >= -1){
		fr = (bin_num * h) - (float(nn) * hrange);
	}
	else{
		fr = 0.5;
	}

	emissiveColor = mix(c2, c1, fr);

	// emissiveColor = vec3(0.235, 0.47, 0.235) * (h + 2.f) / 4.f;

	// if(h >= 0.5){
	// 	emissiveColor = vec3(233.f, 139.f, 60.f) / 255.f * (h - 0.25);
	// }
	// else if(h >= 0.f){
	// 	emissiveColor = vec3(243.f, 235.f, 88.f) / 255.f * (h + 0.25);
	// }
	// else if(h >= -0.5){
	// 	emissiveColor = vec3(173.f, 248.f, 136.f) / 255.f * (h + 0.75);
	// }
	// else{
	// 	emissiveColor = vec3(104.f, 230.f, 250.f) / 255.f * (h + 1.25);
	// }

	//testing
	// float ff = 0.5f;
	// if(h > (0.0 + ff)){
	// 	emissiveColor = vec3(1,0,0);
	// }
	// else if(h < (0.0 + ff)){
	// 	emissiveColor = vec3(0,0,1);
	// }
	// else{
	// 	emissiveColor = vec3(0,1,0);
	// }

	/*This part is the same as your previous assignment. Here we provide a default parameter set for the hard-coded lighting environment. Feel free to change them.*/
	const vec3 LightPosition = vec3(3, 1, 3);
	const vec3 LightIntensity = vec3(20);
	const vec3 ka = 0.1*vec3(1., 1., 1.);
	const vec3 kd = 0.7*vec3(1., 1., 1.);
	const vec3 ks = vec3(2.);
	const float n = 400.0;

	vec3 normal = normalize(vtx_normal.xyz);
	vec3 lightDir = LightPosition - vtx_pos;
	float _lightDist = length(lightDir);
	vec3 _lightDir = normalize(lightDir);
	vec3 _localLight = LightIntensity / (_lightDist * _lightDist);
	vec3 ambColor = ka;
	lightingColor = kd * _localLight * max(0., dot(_lightDir, normal));

	// Your implementation ends here

    return emissiveColor*lightingColor;


}

void main()
{
	frag_color = vec4(get_color(vtx_pos.xy),1.f);
}
