#version 330 core

// Add parts 1a-c first, then implement part 2a

layout (std140) uniform camera
{
	mat4 projection;
	mat4 view;
	mat4 pvm;
	mat4 ortho;
	vec4 position;
};

///////////// Part 1a /////////////////////
/* Create a function that takes in an xy coordinate and returns a 'random' 2d vector. (There is no right answer)
  Feel free to find a hash function online. Use the commented function to check your result */
vec2 hash2(vec2 v)
{
	vec2 rand = vec2(0,0);

	// Your implementation starts here

	//rand  = 50.0 * 1.05 * fract(v * 0.3183099 + vec2(0.71, 0.113));
	//rand = -1.0 + 2 * 1.05 * fract(rand.x * rand.y * (rand.x + rand.y) * rand);

	// taken from https://en.wikipedia.org/wiki/Perlin_noise
	float random = 2920.f * sin(v.x * 21942.f + v.y * 171324.f + 8912.f) * cos(v.x * 23157.f * v.y * 217832.f + 9758.f);
  return vec2(cos(random), sin(random));
	// Your implementation ends here

	return rand;
}

///////////// Part 1b /////////////////////
/*  Using i, f, and m, compute the perlin noise at point v */
float perlin_noise(vec2 v)
{
	float noise = 0;
	// Your implementation starts here
	//   P2------------P3
	//   | \         / |
	//   |  \   /      |
	//   |   V_        |
	//   | /      _    |
	//   P0------------P1
	vec2 v_i = floor(v);
	vec2 v_f = fract(v);

	vec2 p0 = v_i;
	vec2 p1 = v_i + vec2(1.f,0.f);
	vec2 p2 = v_i + vec2(0.f,1.f);
	vec2 p3 = v_i + vec2(1.f,1.f);

	vec2 g0 = hash2(p0);
	vec2 g1 = hash2(p1);
	vec2 g2 = hash2(p2);
	vec2 g3 = hash2(p3);

	vec2 v0 = v - p0;
	vec2 v1 = v - p1;
	vec2 v2 = v - p2;
	vec2 v3 = v - p3;

	float n01 = mix(dot(v0, g0), dot(v1, g1), smoothstep(0.f,1.f,v_f.x));
	float n23 = mix(dot(v2, g2), dot(v3, g3), smoothstep(0.f,1.f,v_f.x));
	noise = mix(n01, n23, smoothstep(0.f,1.f,v_f.y));

	// Your implementation ends here
	return noise;
}

///////////// Part 1c /////////////////////
/*  Given a point v and an int num, compute the perlin noise octave for point v with octave num
	num will be greater than 0 */
float noiseOctave(vec2 v, int num)
{
	float sum = 0;
	// Your implementation starts here
	// sum = perlin_noise(v);

	for(int i = 0; i < num; i++){
		sum += pow(2, -i) * perlin_noise(pow(2, i)*v);
	}
	// Your implementation ends here
	return sum;
}

///////////// Part 2a /////////////////////
/* create a function that takes in a 2D point and returns a height using perlin noise
    There is no right answer. Think about what functions will create what shapes.
    If you want steep mountains with flat tops, use a function like sqrt(noiseOctave(v,num)).
    If you want jagged mountains, use a function like e^(noiseOctave(v,num))
    You can also add functions on top of each other and change the frequency of the noise
    by multiplying v by some value other than 1*/
float height(vec2 v){
	float h = 0;
	// Your implementation starts here
	h = noiseOctave(v, 6) / 2.f;
	float k = 15;
	float hh = 2.f / (1.f + exp(-k * h)) - 1.f;
	if(hh <= -2.f / 13.f){
		return -2.f/13.f;
	}
	// Your implementation ends here
	return h;
}

/*uniform variables*/
uniform float iTime;					////time

/*input variables*/
layout (location=0) in vec4 pos;
layout (location=2) in vec4 normal;
layout (location=3) in vec4 uv;
layout (location=4) in vec4 tangent;

/*output variables*/
out vec3 vtx_pos;		////vertex position in the world space

void main()
{
	vtx_pos = (vec4(pos.xy, height(pos.xy), 1)).xyz;
	gl_Position = pvm * vec4(vtx_pos,1);
}
