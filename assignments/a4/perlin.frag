#version 330 core

#define M1 1597334677U     //1719413*929
#define M2 3812015801U     //140473*2467*11
#define M3 2220083723U     //4297*3613*11*13
#define M4 1449738391U     //7723*6473*29

// Parts 1a, 1b, 1c

layout (std140) uniform camera
{
	mat4 projection;
	mat4 view;
	mat4 pvm;
	mat4 ortho;
	vec4 position;
};

in vec3 vtx_normal;
in vec3 vtx_pos;

out vec4 frag_color;

///////////// Part 1a /////////////////////
/* Create a function that takes in an xy coordinate and returns a 'random' 2d vector. (There is no right answer)
   Feel free to find a hash function online. Use the commented function to check your result */
vec2 hash2(vec2 v)
{
	vec2 rand = vec2(0,0);

	// Your implementation starts here

	//rand  = 50.0 * 1.05 * fract(v * 0.3183099 + vec2(0.71, 0.113));
	//rand = -1.0 + 2 * 1.05 * fract(rand.x * rand.y * (rand.x + rand.y) * rand);

	// taken from https://en.wikipedia.org/wiki/Perlin_noise
	float random = 2920.f * sin(v.x * 21942.f + v.y * 171324.f + 8912.f) * cos(v.x * 23157.f * v.y * 217832.f + 9758.f);
  return vec2(cos(random), sin(random));
	// Your implementation ends here

	return rand;
}

///////////// Part 1b /////////////////////
/*  Using i, f, and m, compute the perlin noise at point v */
float perlin_noise(vec2 v)
{
	float noise = 0;
	// Your implementation starts here
	//   P2------------P3
	//   | \         / |
	//   |  \   /      |
	//   |   V_        |
	//   | /      _    |
	//   P0------------P1
	vec2 v_i = floor(v);
	vec2 v_f = fract(v);

	vec2 p0 = v_i;
	vec2 p1 = v_i + vec2(1.f,0.f);
	vec2 p2 = v_i + vec2(0.f,1.f);
	vec2 p3 = v_i + vec2(1.f,1.f);

	vec2 g0 = hash2(p0);
	vec2 g1 = hash2(p1);
	vec2 g2 = hash2(p2);
	vec2 g3 = hash2(p3);

	vec2 v0 = v - p0;
	vec2 v1 = v - p1;
	vec2 v2 = v - p2;
	vec2 v3 = v - p3;

	float n01 = mix(dot(v0, g0), dot(v1, g1), smoothstep(0.f,1.f,v_f.x));
	float n23 = mix(dot(v2, g2), dot(v3, g3), smoothstep(0.f,1.f,v_f.x));
	noise = mix(n01, n23, smoothstep(0.f,1.f,v_f.y));

	// Your implementation ends here
	return noise;
}

///////////// Part 1c /////////////////////
/*  Given a point v and an int num, compute the perlin noise octave for point v with octave num
	num will be greater than 0 */
float noiseOctave(vec2 v, int num)
{
	float sum = 0;
	// Your implementation starts here
	// sum = perlin_noise(v);

	for(int i = 0; i < num; i++){
		sum += pow(2, -i) * perlin_noise(pow(2, i)*v);
	}
	// Your implementation ends here
	return sum;
}

void main()
{
	vec3 color = 0.5 + 0.5 * (noiseOctave(vtx_pos.xy, 6))  * vec3(1,1,1); // visualize perlin noise
	// vec3 color = vec3(1,0,0);
	frag_color=vec4(color,1.f);
}
