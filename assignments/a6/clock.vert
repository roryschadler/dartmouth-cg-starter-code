//CLOCK VERT

#version 330 core

#define PI 3.14159265

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*input variables*/
layout (location=0) in vec4 pos;		/*vertex position*/
layout (location=1) in vec4 color;		/*vertex color*/
layout (location=2) in vec4 normal;		/*vertex normal*/
layout (location=3) in vec4 uv;			/*vertex uv*/
layout (location=4) in vec4 tangent;	/*vertex tangent*/

uniform mat4 model;						////model matrix
uniform float iTime;
uniform sampler2D tex_albedo;

/*output variables*/

out vec3 vtx_pos;
out vec3 orig_pos;
out vec3 norm;
out vec2 uv2;

mat4 rot_mat(float alpha, float beta, float gamma){
	// alpha is rotation around z
	// beta is rotation around y
	// gamma is rotation around x
	mat4 ret = mat4(vec4(cos(alpha)*cos(beta), sin(alpha)*cos(beta), -sin(beta), 0.f),
									vec4(cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma), sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma), cos(beta)*sin(gamma), 0.f),
									vec4(cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma), sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma), cos(beta)*cos(gamma), 0.f),
									vec4(0.f, 0.f, 0.f, 1.f));
	return ret;
}

mat4 trans_mat(vec3 scale, vec3 displace){
	return mat4(vec4(scale.x, 0.f, 0.f, 0.f),
							vec4(0.f, scale.y, 0.f, 0.f),
							vec4(0.f, 0.f, scale.z, 0.f),
							vec4(displace, 1.f));
}

void main()
{
	float theta = 0.08 * iTime;
	mat4 orbit = mat4(vec4(cos(theta), 0.f, -sin(theta), 0.f), vec4(0.f, 1.f, 0.f, 0.f), vec4(sin(theta), 0.f, cos(theta), 0.f), vec4(0.f,0.f,0.f,1.f));
	float alpha = 0.4;
	float beta = PI*0.7;
	float gamma = -0.5;
	mat4 rot = rot_mat(alpha, beta, gamma);
	mat4 trans = trans_mat(vec3(0.35), vec3(0.15, 0.4, -0.3));
	mat4 my_model = orbit * trans * rot;
	vtx_pos = (my_model * vec4(pos.xyz, 1.f)).xyz;
	orig_pos = pos.xyz;

	norm = (orbit * rot * normal).xyz;
	uv2 = uv.xy;
	gl_Position=pvm * my_model*vec4(pos.xyz,1.f);
}
