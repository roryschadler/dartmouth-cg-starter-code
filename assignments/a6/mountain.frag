//MOUNTAIN FRAG

#version 330 core

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*uniform variables*/
uniform float iTime;					////time
// uniform sampler2D tex_mountain;
uniform sampler2D tex_clock;
uniform sampler2D tex_mug;

//input variables
in vec3 vtx_pos;
in vec3 norm;
in vec3 orig_pos;
in vec2 uv2;
in float revolution_speed;
in mat3 mmm;
flat in float obj_id;

out vec4 frag_color;

// vec3 loc_norm = normalize(texture(tex_normal, uv2).xyz * 2. - vec3(1.f));
// vec3 norm = normalize(mmm * loc_norm.xyz);

float logistic(float x, float L, float k)
{
	return L / (1.f + exp(-k * x));
}

vec2 hash1(vec2 v)
{
	vec2 rand = vec2(0,0);
	// taken from https://en.wikipedia.org/wiki/Perlin_noise
	float random = 2920.f * sin(v.x * 21942.f + 8912.f) * cos(v.x * 23157.f + 9758.f) + 1200.f * v.y;
  return vec2(cos(random), sin(random));
	return rand;
}

vec2 hash2(vec2 v)
{
	vec2 rand = vec2(0,0);
	rand  = 50.0 * 1.05 * fract(v * 0.3183099 + vec2(0.71, 0.113));
	rand = -1.0 + 2 * 1.05 * fract(rand.x * rand.y * (rand.x + rand.y) * rand);
	return rand;
}

float perlin_noise(vec2 uv, int hash)
{
	float noise = 0;
	// Your implementation starts here
	//   P2------------P3
	//   | \         / |
	//   |  \   /      |
	//   |   V_        |
	//   | /      _    |
	//   P0------------P1
	vec2 v = vec2(cos(uv.x), uv.y);
	vec2 v_i = floor(v);
	vec2 v_f = fract(v);

	vec2 p0 = v_i;
	vec2 p1 = v_i + vec2(1.f,0.f);
	vec2 p2 = v_i + vec2(0.f,1.f);
	vec2 p3 = v_i + vec2(1.f,1.f);

	vec2 g0;
	vec2 g1;
	vec2 g2;
	vec2 g3;

	if(hash == 1){
		g0 = hash1(p0);
		g1 = hash1(p1);
		g2 = hash1(p2);
		g3 = hash1(p3);
	}

	else{
		g0 = hash2(p0);
		g1 = hash2(p1);
		g2 = hash2(p2);
		g3 = hash2(p3);
	}

	vec2 v0 = v - p0;
	vec2 v1 = v - p1;
	vec2 v2 = v - p2;
	vec2 v3 = v - p3;

	float n01 = mix(dot(v0, g0), dot(v1, g1), smoothstep(0.f,1.f,v_f.x));
	float n23 = mix(dot(v2, g2), dot(v3, g3), smoothstep(0.f,1.f,v_f.x));
	noise = mix(n01, n23, smoothstep(0.f,1.f,v_f.y));

	return noise;
}

float noiseOctave(vec2 v, int num, int hash)
{
	float sum = 0;
	for(int i = 0; i < num; i++){
		sum += pow(2, -i) * perlin_noise(pow(2, i)*v, hash);
	}
	return sum;
}

float noise(vec2 v, int hash){
	float h = 0;
	h = noiseOctave(v, 12, hash) / 2.f;
	return h;
}

vec3 phong_shader(vec3 l_pos, vec3 l_col, float ka, float kd, float ks, int p){
	vec3 l_dir = normalize(l_pos - vtx_pos.xyz);
	vec4 view_pos = pvm * position;
	vec3 v = normalize(position.xyz - vtx_pos.xyz);
	vec3 r = normalize(-l_dir + 2.f * dot(l_dir, norm) * norm);


	vec3 ambient = ka*l_col;
	vec3 diffuse = kd*l_col*max(dot(norm, l_dir), 0.0);
	vec3 specular = ks*l_col*pow(max(dot(v,r), 0.0), p);

	return ambient + diffuse + specular;
}

vec3 brown = vec3(0.34,0.25,0.12) * 0.34;
vec3 white = vec3(1.f);

vec3 top_color(vec2 uv, float y){
	float n1 = logistic(noise(uv,1), 1.f, 15.f);
	float n2 = logistic(noise(uv,2), 1.f, 8.f);
	vec3 col = vec3(n1 * 0.21,(n2 + n1) * 0.5,n2 * 0.3) * n1 * 0.3;
	if(y > 1.2){
		col = mix(col, vec3(1.f), (y - 1.2)*0.6);
	}
	else if(y < 0.2){
		col = mix(brown, col, y*5.f);
	}
	return col;
}

vec3 bot_color(vec2 uv){
	float n = logistic(noise(uv,2), 0.7, 15.f);
	return vec3(0.34,0.25,0.12)*(n + 0.2);
}

mat3 rot_mat(float theta){
	return mat3(vec3(cos(theta), 0.f, -sin(theta)), vec3(0.f, 1.f, 0.f), vec3(sin(theta), 0.f, cos(theta)));
}

vec3 s6500 = vec3(0.996, 0.976, 1.f); // bright
vec3 s5000 = vec3(0.984, 0.894, 0.812); // orange
vec3 s1850 = vec3(0.941, 0.529, 0.2);
vec3 night = vec3(0.2);

vec3 sun_color(float theta){
	float pi = 3.1415926535793;
	float wrapped_theta = mod(theta, 2.f*pi);
	if(wrapped_theta >= 1.8*pi || wrapped_theta <= 0.2*pi){
		return s6500;
	}
	else if(wrapped_theta >= 1.65*pi || wrapped_theta <= 0.35*pi){
		return mix(s5000, s6500, smoothstep(0.f, 1.f, (abs(wrapped_theta/pi - 1.f) - 0.65)/0.15));
	}
	else if(wrapped_theta >= 1.55*pi || wrapped_theta <= 0.45*pi){
		return mix(s1850, s5000, smoothstep(0.f, 1.f, (abs(wrapped_theta/pi - 1.f) - 0.55)/0.1));
	}
	else if(wrapped_theta >= 1.45*pi || wrapped_theta <= 0.55*pi){
		return mix(night, s1850, smoothstep(0.f, 1.f, (abs(wrapped_theta/pi - 1.f) - 0.45)/0.1));
	}
	else{
		return night;
	}
}

vec3 mountain_color(){
	vec3 col = vec3(0.f);
	if(vtx_pos.y >= 0.f){// + logistic(noise(uv2,1), 0.1, 15.f)){
		col = top_color(uv2, vtx_pos.y);
	}
	else{
		vec2 uv_edit = normalize(vec2(uv2.x * 2.f, uv2.y * 0.2));
		col = bot_color(uv2);
	}

	return col;
}

void main()
{
	// vec3 col = vec3(0.f);

	float orbit_speed = -0.3;
	vec3 sun_pos = rot_mat(orbit_speed * iTime) * vec3(-10.f, -1.894, -1.121);
	float sun_angle = acos(dot(normalize(sun_pos), normalize(vtx_pos)));
	vec3 sun_col = sun_color(orbit_speed * iTime - (atan(vtx_pos.x,vtx_pos.z) + sun_angle));
	float sun_ka = 1.8f;
	float sun_kd = 1.5f;
	float sun_ks = 0.8f;
	int sun_p = 2;

	vec3 col = mountain_color();

	// col = vec3(1.f);
	frag_color = vec4(phong_shader(sun_pos, sun_col, sun_ka, sun_kd, sun_ks, sun_p) * col, 1.f);
	// frag_color = vec4(col, 1.f);
}
