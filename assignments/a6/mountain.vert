//MOUNTAIN VERT

#version 330 core

#define PI 3.14159265

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*input variables*/
layout (location=0) in vec4 pos;		/*vertex position*/
layout (location=1) in vec4 color;		/*vertex color*/
layout (location=2) in vec4 normal;		/*vertex normal*/
layout (location=3) in vec4 uv;			/*vertex uv*/
layout (location=4) in vec4 tangent;	/*vertex tangent*/

uniform mat4 model;						////model matrix
uniform float iTime;					////time
uniform sampler2D tex_rand;

/*output variables*/
out vec3 vtx_pos;
out vec3 norm;
out vec3 vtx_tan;
out vec3 orig_pos;
out vec2 uv2;
out vec2 my_uv;
out float revolution_speed;
out mat3 mmm;
flat out float obj_id;

float logistic(float x, float L, float k)
{
	return L / (1.f + exp(-k * x));
}

vec2 hash1(vec2 v)
{
	vec2 rand = vec2(0,0);
	// taken from https://en.wikipedia.org/wiki/Perlin_noise
	float random = 2920.f * sin(v.x * 21942.f + 8912.f) * cos(v.x * 23157.f + 9758.f) + 1200.f * v.y;
  return vec2(cos(random), sin(random));
	return rand;
}

vec2 hash2(vec2 v)
{
	vec2 rand = vec2(0,0);
	rand  = 50.0 * 1.05 * fract(v * 0.3183099 + vec2(0.71, 0.113));
	rand = -1.0 + 2 * 1.05 * fract(rand.x * rand.y * (rand.x + rand.y) * rand);
	return rand;
}

float rand_from_tex(vec2 v){
	return texture(tex_rand, v).x;
}

///////////// Part 1b /////////////////////
/*  Using i, f, and m, compute the perlin noise at point v */
float perlin_noise(vec2 uv, int hash)
{
	float noise = 0;
	// Your implementation starts here
	//   P2------------P3
	//   | \         / |
	//   |  \   /      |
	//   |   V_        |
	//   | /      _    |
	//   P0------------P1
	vec2 v = vec2(cos(uv.x), uv.y);
	vec2 v_i = floor(v);
	vec2 v_f = fract(v);

	vec2 p0 = v_i;
	vec2 p1 = v_i + vec2(1.f,0.f);
	vec2 p2 = v_i + vec2(0.f,1.f);
	vec2 p3 = v_i + vec2(1.f,1.f);

	vec2 g0;
	vec2 g1;
	vec2 g2;
	vec2 g3;

	if(hash == 1){
		g0 = hash1(p0);
		g1 = hash1(p1);
		g2 = hash1(p2);
		g3 = hash1(p3);
	}

	else{
		g0 = hash2(p0);
		g1 = hash2(p1);
		g2 = hash2(p2);
		g3 = hash2(p3);
	}

	vec2 v0 = v - p0;
	vec2 v1 = v - p1;
	vec2 v2 = v - p2;
	vec2 v3 = v - p3;

	float n01 = mix(dot(v0, g0), dot(v1, g1), smoothstep(0.f,1.f,v_f.x));
	float n23 = mix(dot(v2, g2), dot(v3, g3), smoothstep(0.f,1.f,v_f.x));
	noise = mix(n01, n23, smoothstep(0.f,1.f,v_f.y));

	return noise;
}

///////////// Part 1c /////////////////////
/*  Given a point v and an int num, compute the perlin noise octave for point v with octave num
	num will be greater than 0 */
float noiseOctave(vec2 v, int num, int hash)
{
	float sum = 0;
	// Your implementation starts here
	// sum = perlin_noise(v);

	for(int i = 0; i < num; i++){
		sum += pow(2, -i) * perlin_noise(pow(2, i)*v, hash);
	}
	// Your implementation ends here
	return sum;
}

float noise(vec2 v, int hash){
	float h = 0;
	// Your implementation starts here
	h = noiseOctave(v, 15, hash) / 2.f;
	// h = logistic(h, 1.f, 15.f);
	// Your implementation ends here
	return h;
}

vec3 compute_normal(vec3 v, float d)
{
	vec3 normal_vector = vec3(0,0,0);
	// Your implementation starts here

	//     V3
	//     ^
	//     |
	//V2<--v-->V1
	//     |
	//     V
	//     V4

	// vec3 v1 = vec3(v.x + d, v.y, logistic(noise(vec2(v.x + d, v.y)), 1.f, 15.f));
	// vec3 v2 = vec3(v.x - d, v.y, logistic(noise(vec2(v.x - d, v.y)), 1.f, 15.f));
	// vec3 v3 = vec3(v.x, v.y + d, logistic(noise(vec2(v.x, v.y + d)), 1.f, 15.f));
	// vec3 v4 = vec3(v.x, v.y - d, logistic(noise(vec2(v.x, v.y - d)), 1.f, 15.f));

	vec3 v1 = vec3(v.x + d, v.y, rand_from_tex(vec2(v.x + d, v.y)));
	vec3 v2 = vec3(v.x - d, v.y, rand_from_tex(vec2(v.x - d, v.y)));
	vec3 v3 = vec3(v.x, v.y + d, rand_from_tex(vec2(v.x, v.y + d)));
	vec3 v4 = vec3(v.x, v.y - d, rand_from_tex(vec2(v.x, v.y - d)));

	normal_vector = normalize(cross(v1 - v2, v3 - v4));
	// Your implementation ends here
	return normal_vector;
}

float gauss(float x, float mean, float stdev){
	return exp(stdev * pow(x - mean, 2));
}

float flip_sign(vec2 v){
	float r = texture(tex_rand, v).x;
	if(r > 0.65){
		return 1.f;
	}
	else if(r < 0.35){
		return -1.f;
	}
	else{
		return 0.f;
	}
}

void main()
{
	float theta = 0.08 * iTime;
	mat4 orbit = mat4(vec4(cos(theta), 0.f, -sin(theta), 0.f), vec4(0.f, 1.f, 0.f, 0.f), vec4(sin(theta), 0.f, cos(theta), 0.f), vec4(0.f,0.f,0.f,1.f));
	my_uv = vec2(atan(pos.x,pos.z),pos.y);
	uv2 = uv.xy;

	vec3 tt = tangent.xyz;
	vec3 nn = normal.xyz;
	vec3 bb = cross(nn, tt);
	mat3 TBN = mat3(tt,bb,nn);
	mat3 TBN_i = inverse(TBN);

	vec3 loc_norm = TBN_i * normal.xyz;

	float max_strength = 0.15;
	// float bump_strength = noise(my_uv.xy)*(2.f*max_strength/1.5) - max_strength;
	float bump_strength = rand_from_tex(uv.xy) * flip_sign(uv.xy);
	// bump_strength = logistic(2.f - pos.y, 1.f, 2.f) * bump_strength;

	bump_strength *= max_strength * (gauss(pos.y, 0.8, -8.f) + gauss(pos.y, -0.8, -8.f) + 0.2*(gauss(pos.y, 1.f, -22.2222) + gauss(pos.y, -1.f, -22.2222)));

	// bump_strength = bump_strength * max_strength - 2.f * max_strength;

	vec3 bump_norm = compute_normal(TBN_i * pos.xyz, 0.01);
	norm = (orbit * vec4(normalize(bump_norm + normal.xyz), 1.f)).xyz;
	vtx_pos = (orbit * vec4(pos.xyz + bump_strength * norm, 1.f)).xyz;

	obj_id = 0.f; // mountain

	gl_Position=pvm*model*vec4(vtx_pos.xyz,1.f);
}
