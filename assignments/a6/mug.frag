//MUG FRAG

#version 330 core

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*uniform variables*/
uniform float iTime;					////time
uniform sampler2D tex_mug;

in vec3 vtx_pos;
in vec3 norm;
// in vec3 col;
in vec2 uv2;

out vec4 frag_color;

vec3 phong_shader(vec3 l_pos, vec3 l_col, float ka, float kd, float ks, int p){
	vec3 l_dir = normalize(l_pos - vtx_pos.xyz);
	vec4 view_pos = pvm * position;
	vec3 v = normalize(position.xyz - vtx_pos.xyz);
	vec3 r = normalize(-l_dir + 2.f * dot(l_dir, norm) * norm);


	vec3 ambient = ka*l_col;
	vec3 diffuse = kd*l_col*max(dot(norm, l_dir), 0.0);
	vec3 specular = ks*l_col*pow(max(dot(v,r), 0.0), p);

	return ambient + diffuse + specular;
}

mat3 rot_mat(float theta){
	return mat3(vec3(cos(theta), 0.f, -sin(theta)), vec3(0.f, 1.f, 0.f), vec3(sin(theta), 0.f, cos(theta)));
}

vec3 s6500 = vec3(0.996, 0.976, 1.f); // bright
vec3 s5000 = vec3(0.984, 0.894, 0.812); // orange
vec3 s1850 = vec3(0.941, 0.529, 0.2);
vec3 night = vec3(0.1);

vec3 sun_color(float theta){
	float pi = 3.1415926535793;
	float wrapped_theta = mod(theta, 2.f*pi);
	if(wrapped_theta >= 1.8*pi || wrapped_theta <= 0.2*pi){
		return s6500;
	}
	else if(wrapped_theta >= 1.65*pi || wrapped_theta <= 0.35*pi){
		return mix(s5000, s6500, smoothstep(0.f, 1.f, (abs(wrapped_theta/pi - 1.f) - 0.65)/0.15));
	}
	else if(wrapped_theta >= 1.55*pi || wrapped_theta <= 0.45*pi){
		return mix(s1850, s5000, smoothstep(0.f, 1.f, (abs(wrapped_theta/pi - 1.f) - 0.55)/0.1));
	}
	else if(wrapped_theta >= 1.45*pi || wrapped_theta <= 0.55*pi){
		return mix(night, s1850, smoothstep(0.f, 1.f, (abs(wrapped_theta/pi - 1.f) - 0.45)/0.1));
	}
	else{
		return night;
	}
}

void main()
{
	float orbit_speed = -0.3;
	vec3 sun_pos = rot_mat(orbit_speed * iTime) * vec3(-10.f, -1.894, -1.121);
	float sun_angle = acos(dot(normalize(sun_pos), normalize(vtx_pos)));
	vec3 sun_col = sun_color(orbit_speed * iTime - (atan(vtx_pos.x,vtx_pos.z) + sun_angle));
	float sun_ka = 1.8f;
	float sun_kd = 1.5f;
	float sun_ks = 1.8f;
	int sun_p = 15;

	// vec3 col = texture(tex_mug, uv2).rgb;
	vec3 col = vec3(0.984, 0.98, 0.82) * 0.3;
	frag_color = vec4(phong_shader(sun_pos, sun_col, sun_ka, sun_kd, sun_ks, sun_p) * col, 1.f);
}
