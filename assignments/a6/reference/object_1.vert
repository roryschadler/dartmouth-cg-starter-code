/*This is your first vertex shader!*/

#version 330 core

#define PI 3.14159265

/*default camera matrices. do not modify.*/
layout (std140) uniform camera
{
	mat4 projection;	/*camera's projection matrix*/
	mat4 view;			/*camera's view matrix*/
	mat4 pvm;			/*camera's projection*view*model matrix*/
	mat4 ortho;			/*camera's ortho projection matrix*/
	vec4 position;		/*camera's position in world space*/
};

/*input variables*/
layout (location=0) in vec4 pos;		/*vertex position*/
layout (location=1) in vec4 color;		/*vertex color*/
layout (location=2) in vec4 normal;		/*vertex normal*/
layout (location=3) in vec4 uv;			/*vertex uv*/
layout (location=4) in vec4 tangent;	/*vertex tangent*/

uniform mat4 model;						////model matrix
uniform float iTime;

/*output variables*/

void main()
{
	float theta = 0.2 * iTime;
	mat4 rot_mat = mat4(vec4(cos(theta), 0.f, -sin(theta), 0.f), vec4(0.f, 1.f, 0.f, 0.f), vec4(sin(theta), 0.f, cos(theta), 0.f), vec4(0.f,0.f,0.f,1.f));
	gl_Position=pvm * rot_mat * model*vec4(pos.xyz,1.f);
}
